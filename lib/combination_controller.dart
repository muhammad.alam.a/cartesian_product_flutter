import 'package:get/get.dart';

import 'variant_controller.dart';

class CombinationController extends GetxController {
  final List<Variant> variants;
  late final List<Combination> combinations;

  CombinationController({required this.variants});

  @override
  void onInit() {
    super.onInit();

    combinations = permutations(variants);
  }

  /// https://stackoverflow.com/a/57883482/2537616
  List<Combination> permutations(List<Variant> elements) {
    List<Combination> perms = [];
    generatePermutations(elements, perms, 0, []);
    return perms;
  }

  void generatePermutations(List<Variant> lists, List<Combination> result,
      int depth, List<Option> current) {
    if (depth == lists.length) {
      final name = current.map((e) => e.name).join();
      final combination = Combination(name, current);
      result.add(combination);
      return;
    }

    for (int i = 0; i < lists[depth].options.length; i++) {
      generatePermutations(
        lists,
        result,
        depth + 1,
        [...current, lists[depth].options[i]],
      );
    }
  }
}

class Combination {
  final String name;
  final List<Option> options;

  Combination(this.name, this.options);
}
