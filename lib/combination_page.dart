import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'combination_controller.dart';

class CombinationPage extends StatelessWidget {
  const CombinationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CombinationController>(
      init: CombinationController(variants: Get.arguments),
      builder: (ctr) {
        return Theme(
          data: ThemeData.light(),
          child: Scaffold(
            appBar: AppBar(
              title: const Text("Combination Page"),
            ),
            body: ListView(
              children: ctr.combinations
                  .map((c) => ListTile(
                        contentPadding: const EdgeInsets.all(8),
                        shape: const Border(
                            bottom: BorderSide(color: Colors.grey)),
                        title: Text(c.name),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children:
                              c.options.map((vn) => Text(vn.name)).toList(),
                        ),
                        onTap: () {
                          Get.to(
                            () => const CombinationPage(),
                            arguments: ctr.variants,
                          );
                        },
                      ))
                  .toList(),
            ),
          ),
        );
      },
    );
  }
}
