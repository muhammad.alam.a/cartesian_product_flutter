import 'package:get/get.dart';

class VariantController extends GetxController {
  final List<Variant> variants = [];

  @override
  void onInit() {
    super.onInit();

    variants.add(Variant("Warna", [
      Option("Biru"),
      Option("Merah"),
      // Variation("Kuning"),
    ]));
    variants.add(Variant("Size", [
      Option("S"),
      Option("M"),
      Option("L"),
    ]));
    // variants.add(Variant("Sex", [
    //   Variation("Male"),
    //   Variation("Female"),
    // ]));
  }
}

class Variant {
  final String name;
  final List<Option> options;

  Variant(this.name, this.options);
}

class Option {
  String name;

  Option(this.name);
}
