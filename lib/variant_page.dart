import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'combination_page.dart';
import 'variant_controller.dart';

class VariantPage extends StatelessWidget {
  const VariantPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<VariantController>(
      init: VariantController(),
      builder: (ctr) {
        return Theme(
          data: ThemeData.light(),
          child: Scaffold(
            appBar: AppBar(
              title: const Text("Variant Page"),
            ),
            body: Column(
              children: [
                Expanded(
                  child: ListView(
                    children: ctr.variants
                        .map((vt) => ListTile(
                              contentPadding: const EdgeInsets.all(8),
                              shape: const Border(
                                  bottom: BorderSide(color: Colors.grey)),
                              title: Text(vt.name),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: vt.options
                                    .map((vn) => Text(vn.name))
                                    .toList(),
                              ),
                            ))
                        .toList(),
                  ),
                ),
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    child: const Text("Next"),
                    onPressed: () {
                      Get.to(
                        () => const CombinationPage(),
                        arguments: ctr.variants,
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
